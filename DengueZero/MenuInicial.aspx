﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageWeb.Master" AutoEventWireup="true" CodeBehind="MenuInicial.aspx.cs" Inherits="DengueZero.MenuInicial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div id="topoMenu">
        <nav class="navbar navbar-inverse">
            <<div class="container">
                <ol>
                    <li class="opcoesTopo">
                        <span class="glyphicon glyphicon-home icoTopo" aria-hidden="true"></span>
                        <a class="txtMenu" href="home.html">Inicio</a>
                    </li>
                    <li class="opcoesTopo">
                        <span class="glyphicon glyphicon-user icoTopo" aria-hidden="true"></span>
                        <a class="txtMenu" href="cadastroUsuario.html">Cadastro Usuário</a>
                    </li>
                </ol>
            </div>
        </nav>
    </div>


    <!-- Div responsavel por manter o layout  -->
    <div class="container">
        <nav class="navbar">
            <h4 id="txtSubMenu">DENGUE ZERO</h4>
        </nav>

        <!-- Div onde fica a busca pelo endereço  -->
        <div id="barraSub">
            <nav id="barraSubMenu" class="navbar navbar-inverse">
                <ol>
                    <li class="opcoesTopo">
                        <a class="txtMenu" href="#">Busca Endereço:</a>
                    </li>
                    <li class="opcoesTopo">
                        <span class="glyphicon glyphicon-pencil icoTopo" aria-hidden="true"></span>
                        <input type="text" name="iptBusca" placeholder="Digite o Local" id="iptBusca" />
                    </li>
                </ol>
            </nav>
        </div>
        <br />
        <div id="mapaGoogle">
            <img src="DengueZero/img/Mapa.jpg" />
        </div>
    <br /><br />
    </div>

    <div class="rodape">
        <nav class="navbar navbar-inverse">
            <<div class="container">
                <ol>
                    <li class="opcoesTopo">
                        <a class="txtMenu">Integrantes:</a>
                    </li>
                    <li class="opcoesTopo">
                        <a class="txtMenu">Walisson Andrade, Leonardo de Cássio, Pedro Paulo, Sergio Lins</a>
                    </li>
                </ol>
            </div>
        </nav>
    </div>





    <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
    <script src="js/bootstrap.min.js"></script>
</asp:Content>