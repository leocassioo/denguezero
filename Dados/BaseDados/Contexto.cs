﻿using Criptografar;
using System.Data.Entity;
using System.Diagnostics;
using System.Web.Configuration;

namespace Dados.BaseDados
{
    /// <summary>
    /// Classe que faz a interligação do banco de dados com a aplicação
    /// </summary>
    public class Contexto : DbContext
    {
        /// <summary>
        /// Construtor default, pega a connection string no Web.config e decripta
        /// </summary>
        public Contexto()
            : base(CriptografiaConnectionString.DecriptarConnectionString(WebConfigurationManager.AppSettings["settings"]))
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #if DEBUG

            Database.Log = s => Debug.Write(s);

            #endif
        }
    }
}